import random

failureX = 0
failureY = 0

def initBlock():
	if (failureRate == 0.01):
		failureX = random.randint(0, 899)
		failureY = random.randint(0, 899)
	elif (failureRate == 0.005):
		failureX = random.randint(0, 899)
		failureY = random.randint(0, 949)
	elif (failureRate == 0.002):
		failureX = random.randint(0, 949)
		failureY = random.randint(0, 959)
	elif (failureRate == 0.001):
		failureX = random.randint(0, 949)
		failureY = random.randint(0, 979)

def blockPattern(testCaseX, testCaseY, failureRate):
	if (failureRate == 0.01):
		if (testCaseX >= failureX and testCaseX <= failureX + 99 
			and testCaseY >= failureY and testCaseY <= failureY + 99):
			return True
	if (failureRate == 0.005):
		if (testCaseX >= failureX and testCaseX <= failureX + 99
			and testCaseY >= failureY and testCaseY <= failureY + 49):
			return True
	if (failureRate == 0.002):
		if (testCaseX >= failureX and testCaseX <= failureX + 49
			and testCaseY >= failureY and testCaseY <= failureY + 39):
			return True
	if (failureRate == 0.001):
		if (testCaseX >= failureX and testCaseX <= failureX + 49
			and testCaseY >= failureY and testCase <= failureY + 19):
			return True
	return False
