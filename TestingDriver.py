import time
import random
import math

def printMainMenu():
	print("\n1. Set test method")
	print("2. Set failure rate")
	print("3. Run the test")
	print("4. Display current setting")
	print("5. Quit")

def printTestMethodMenu():
	print("\n1. Static partitioning")
	print("2. Distance + Static partitioning")
	
def printPatternMenu():
	print("\n1. Block pattern")
	print("2. Strip pattern")
	print("3. Point pattern")

def printFailureRateMenu():
	print("\n1. 0.01")
	print("2. 0.005")
	print("3. 0.002")
	print("4. 0.001")

def printCurrentSetting():
	print("\nCurrent setting:")
	if testMethod == 1:
		print("\tTest method: Static partitioning")
	else:
		print("\tTest method: Distance + Static partitioning")
	if testPattern == 1:
		print("\tTest pattern: Block pattern")
	elif testPattern == 2:
		print("\tTest pattern: Strip pattern")
	else:
		print("\tTest pattern: Point pattern")
	print("\tFailure rate: " + str(failureRate))

def blockPattern(cellX, cellY, testCaseX, testCaseY, failureRate):
	if (failureRate == 0.01):
		if (cellX >= 25 and cellX <= 34):
			if (cellY >= 55 and cellY <= 64):
				return True
	elif (failureRate == 0.005):
		if (cellX >= 25 and cellX <= 31):
			if (cellY >= 55 and cellY <= 61):
				return True
			if (cellY == 54):
				return True
	elif (failureRate == 0.002):
		if (cellX >= 25 and cellX <= 28):
			if (cellY >= 55 and cellY <= 58):
				return True
			if (cellY == 59):
				return True
	elif (failureRate == 0.001):
		if (cellX >= 25 and cellX <= 27):
			if (cellY >= 55 and cellY <= 57):
				return True
		if (cellX == 28):
			if (cellY == 59):
				return True
	return False

def stripPattern(cellX, cellY, testCaseX, testCaseY, failureRate):
	
	if (failureRate == 0.01):
		if (testCaseX + testCaseY >= 400 and testCaseX + testCaseY <= 424 and testCaseX < 400 and testCaseY < 425):
			return True
	elif (failureRate == 0.005):
		if (testCaseX + testCaseY >= 500 and testCaseX + testCaseY <= 509 and testCaseX < 500 and testCaseY < 510):
			return True
	elif (failureRate == 0.002):
		if (testCaseX + testCaseY >= 500 and testCaseX + testCaseY <= 503 and testCaseX < 500 and testCaseY < 504):
			return True
	elif (failureRate == 0.001):
		if (testCaseX + testCaseY == 1000 and testCaseX < 1000):
			return True
	return False

def pointPattern(cellX, cellY, testCaseX, testCaseY, failureRate):
	if (failureRate == 0.01):
		if (cellX % 10 == 5 and cellY % 10 == 0):
			return True
	elif (failureRate == 0.005):
		pointList = []
		for i in range(70):
			pointList.append(4 + 14 * i)
		if (testCaseX in pointList):
			if (testCaseY in pointList):
				return True
		if (testCaseX == 984):
			if (testCaseY in pointList):
				return True
		if (testCaseX == 998):
			if (testCaseY in pointList[:30]):
				return True
	elif (failureRate == 0.002):
		pointList = []
		for i in range(44):
			pointList.append(18 + 22 * i)
		if (testCaseX in pointList):
			if (testCaseY in pointList):
				return True
		if (testCaseX == 986):
			if (testCaseY in pointList):
				return True
		if (testCaseX == 999):
			if (testCaseY in pointList[:20]):
				return True
	elif (failureRate == 0.001):
		pointList = []
		for i in range(30):
			pointList.append(2 + 28 * i)
		if (testCaseX in pointList):
			if (testCaseY in pointList):
				return True
		if (testCaseX == 842 or testCaseX == 870 or testCaseX == 898):
			if (testCaseY in pointList):
				return True
		if (testCaseX == 926):
			if (testCaseY in pointList[:10]):
				return True

	return False

def distanceAlg(cellX, cellY, selected_list):
	candidate_set = {}
	total_number_of_candidates = 10

	best_distance = -1

	for i in range(total_number_of_candidates):
		testCaseTuple = (random.randint(0, 99), random.randint(0, 99))

		while testCaseTuple in candidate_set:
			testCaseTuple = (random.randint(0, 99), random.randint(0, 99))

		candidate_set[testCaseTuple] = "Blank"

		min_candidate_distance = 1414214

		for j in selected_list:
			curDistance = findDistance(cellX, cellY, j, testCaseTuple)
			
			if (min_candidate_distance > curDistance):
				min_candidate_distance = curDistance
#			min_candidate_distance = min(min_candidate_distance, findDistance(cellX, cellY, j, testCaseTuple))

		if (best_distance < min_candidate_distance):
			best_data = testCaseTuple
			best_distance = min_candidate_distance
	best_data = (cellX * 100 + best_data[0], cellY * 100 + best_data[1])
#	print(best_data[0])
#	print(best_data[1])
	return best_data

def findDistance(cellX, cellY, j, testCaseTuple):
	testCaseX = cellX * 100 + testCaseTuple[0]
	testCaseY = cellY * 100 + testCaseTuple[1]

	return math.sqrt(math.pow(testCaseX - j[0], 2) + pow(testCaseY - j[1], 2))

def test(method, pattern, n, failureRate):
	count = 0;
	
	white_cells_dict = {}
	white_cells_count = 0

	green_cells_dict = {}
	green_cells_count = 0

	yellow_cells_dict = {}
	yellow_cells_count = 0

	red_cells_dict = {}
	red_cells_count = 0

	if (method != 1):
		selected_list = []

	matrix = [[0 for x in range(n)] for x in range(n)] 

	for i in range(n):
		for j in range(n):
			matrix[i][j] = 0
			white_cells_dict[(i, j)] = 0

	white_cells_count = n * n

	while (white_cells_count != 0 or green_cells_count != 0 or yellow_cells_count != 0):
		#print(count)
		if (white_cells_count != 0):
			key = random.choice(list(white_cells_dict.keys()))
			color = white_cells_dict.get(key)
		elif (green_cells_count != 0):
			key = random.choice(list(green_cells_dict.keys()))
			color = green_cells_dict.get(key)
		elif (yellow_cells_count  != 0):
			key = random.choice(list(yellow_cells_dict.keys()))
			color = yellow_cells_dict.get(key)

		if (method == 1):
			testCaseX = random.randint(0, 99) + key[0] * 100
			testCaseY = random.randint(0, 99) + key[1] * 100
		else:
			testCaseTuple = distanceAlg(key[0], key[1], selected_list)
			selected_list.append(testCaseTuple)
			testCaseX = testCaseTuple[0]
			testCaseY = testCaseTuple[1]

		if (pattern == 1):
			result = blockPattern(key[0], key[1], testCaseX, testCaseY, failureRate)
		elif (pattern == 2):
			result = stripPattern(key[0], key[1], testCaseX, testCaseY, failureRate)
		else:
			result = pointPattern(key[0], key[1], testCaseX, testCaseY, failureRate)

		if (result):
			return count
		else:
			matrix[key[0]][key[1]] = 3
			red_cells_dict[key] = 3
			red_cells_count += 1

			if (color == 0):
				white_cells_dict.pop(key)
				white_cells_count -= 1
			elif (color == 1):
				green_cells_dict.pop(key)
				green_cells_count -= 1
			elif (color == 2):
				yellow_cells_dict.pop(key)
				yellow_cells_count -= 1
		
		# top left
		if (key[0] - 1 >= 0 and key[1] + 1 < n):
			if (matrix[key[0] - 1][key[1] + 1] == 0):
				matrix[key[0] - 1][key[1] + 1] = 1
				white_cells_dict.pop((key[0] - 1, key[1] + 1))
				white_cells_count -= 1
				green_cells_dict[(key[0] - 1, key[1] + 1)] = 1
				green_cells_count += 1
			elif (matrix[key[0] - 1][key[1] + 1] == 1):
				matrix[key[0] - 1][key[1] + 1] = 2
				green_cells_dict.pop((key[0] - 1, key[1] + 1))
				green_cells_count -= 1
				yellow_cells_dict[(key[0] - 1, key[1] + 1)] = 2
				yellow_cells_count += 1
		# top
		if (key[1] + 1 < n):
			if (matrix[key[0]][key[1] + 1] == 0):
				matrix[key[0]][key[1] + 1] = 1
				white_cells_dict.pop((key[0], key[1] + 1))
				white_cells_count -= 1
				green_cells_dict[(key[0], key[1] + 1)] = 1
				green_cells_count += 1
			elif (matrix[key[0]][key[1] + 1] == 1):
				matrix[key[0]][key[1] + 1] = 2
				green_cells_dict.pop((key[0], key[1] + 1))
				green_cells_count -= 1
				yellow_cells_dict[(key[0], key[1] + 1)] = 2
				yellow_cells_count += 1

		# top right
		if (key[0] + 1 < n and key[1] + 1 < n):
			if (matrix[key[0] + 1][key[1] + 1] == 0):
				matrix[key[0] + 1][key[1] + 1] = 1
				white_cells_dict.pop((key[0] + 1, key[1] + 1))
				white_cells_count -= 1
				green_cells_dict[(key[0] + 1, key[1] + 1)] = 1
				green_cells_count += 1
			elif (matrix[key[0] + 1][key[1] + 1] == 1):
				matrix[key[0] + 1][key[1] + 1] = 2
				green_cells_dict.pop((key[0] + 1, key[1] + 1))
				green_cells_count -= 1
				yellow_cells_dict[(key[0] + 1, key[1] + 1)] = 2
				yellow_cells_count += 1

		# right
		if (key[0] + 1 < n):
			if (matrix[key[0] + 1][key[1]] == 0):
				matrix[key[0] + 1][key[1]] = 1
				white_cells_dict.pop((key[0] + 1, key[1]))
				white_cells_count -= 1
				green_cells_dict[(key[0] + 1, key[1])] = 1
				green_cells_count += 1
			elif (matrix[key[0] + 1][key[1]] == 1):
				matrix[key[0] + 1][key[1]] = 2
				green_cells_dict.pop((key[0] + 1, key[1]))
				green_cells_count -= 1
				yellow_cells_dict[(key[0] + 1, key[1])] = 2
				yellow_cells_count += 1

		# right bottom
		if (key[0] + 1 < n and key[1] - 1 >= 0):
			if (matrix[key[0] + 1][key[1] - 1] == 0):
				matrix[key[0] + 1][key[1] - 1] = 1
				white_cells_dict.pop((key[0] + 1, key[1] - 1))
				white_cells_count -= 1
				green_cells_dict[(key[0] + 1, key[1] - 1)] = 1
				green_cells_count += 1
			elif (matrix[key[0] + 1][key[1] - 1] == 1):
				matrix[key[0] + 1][key[1] - 1] = 2
				green_cells_dict.pop((key[0] + 1, key[1] - 1))
				green_cells_count -= 1
				yellow_cells_dict[(key[0] + 1, key[1] - 1)] = 2
				yellow_cells_count += 1

		# bottom
		if (key[1] - 1 >= 0):
			if (matrix[key[0]][key[1] - 1] == 0):
				matrix[key[0]][key[1] - 1] = 1
				white_cells_dict.pop((key[0], key[1] - 1))
				white_cells_count -= 1
				green_cells_dict[(key[0], key[1] - 1)] = 1
				green_cells_count += 1
			elif (matrix[key[0]][key[1] - 1] == 1):
				matrix[key[0]][key[1] - 1] = 2
				green_cells_dict.pop((key[0], key[1] - 1))
				green_cells_count -= 1
				yellow_cells_dict[(key[0], key[1] - 1)] = 2
				yellow_cells_count += 1

		# left bottom
		if (key[0] - 1 >= 0 and key[1] - 1 >= 0):
			if (matrix[key[0] - 1][key[1] - 1] == 0):
				matrix[key[0] - 1][key[1] - 1] = 1
				white_cells_dict.pop((key[0] - 1, key[1] - 1))
				white_cells_count -= 1
				green_cells_dict[(key[0] - 1, key[1] - 1)] = 1
				green_cells_count += 1
			elif (matrix[key[0] - 1][key[1] - 1] == 1):
				matrix[key[0] - 1][key[1] - 1] = 2
				green_cells_dict.pop((key[0] - 1, key[1] - 1))
				green_cells_count -= 1
				yellow_cells_dict[(key[0] - 1, key[1] - 1)] = 2
				yellow_cells_count += 1

		# left
		if (key[0] - 1 >= 0):
			if (matrix[key[0] - 1][key[1]] == 0):
				matrix[key[0] - 1][key[1]] = 1
				white_cells_dict.pop((key[0] - 1, key[1]))
				white_cells_count -= 1
				green_cells_dict[(key[0] - 1, key[1])] = 1
				green_cells_count += 1
			elif (matrix[key[0] - 1][key[1]] == 1):
				matrix[key[0] - 1][key[1]] = 2
				green_cells_dict.pop((key[0] - 1, key[1]))
				green_cells_count -= 1
				yellow_cells_dict[(key[0] - 1, key[1])] = 2
				yellow_cells_count += 1

		count += 1

		if (red_cells_count == n * n):
			print(count)
			white_cells_dict = {}
			green_cells_dict = {}
			green_cells_count = 0
			yellow_cells_dict = {}
			yellow_cells_count = 0
			red_cells_dict = {}
			red_cells_count = 0
			for i in range(n):
				for j in range(n):
					matrix[i][j] = 0
					white_cells_dict[(i, j)] = 0
			count = 0
			white_cells_count = n * n

testMethod = 1
testPattern = 1
numTestCase = 1000000
failureRate = 0.01

printMainMenu()

userIn = input("\nSelect menu: ")

while userIn != "5":
	if userIn == "1":
		printTestMethodMenu()

		userIn = input("\nSelect menu: ")

		if userIn == "1":
			testMethod = 1
		elif userIn == "2":
			testMethod = 2
		else:
			print("Invalid input. Set to static partitioning by default")
			testMethod = 1
			
		printPatternMenu()
			
		userIn = input("\nChoose pattern: ")
			
		if userIn == "1":
			testPattern = 1
		elif userIn == "2":
			testPattern = 2
		elif userIn == "3":
			testPattern = 3
		else:
			print("Invalid input. Set to block pattern by default")	
			testPattern = 1

	elif userIn == "2":
		printFailureRateMenu()

		userIn = input("\nChoose failure rate: ")

		if userIn == "1":
			failureRate = 0.01
		elif userIn == "2":
			failureRate = 0.005
		elif userIn == "3":
			failureRate = 0.002
		elif userIn == "4":
			failureRate = 0.001
		else:
			print("Invalid input. Set failure rate to 0.01 by default.")
			failureRate = 0.01

	elif userIn == "3":
		if testMethod == 1 and testPattern == 1 and failureRate == 0.01:
			outfile = open ("Static_partitioning_block_01_output", "w")
		elif testMethod == 1 and testPattern == 1 and failureRate == 0.005:
			outfile = open ("Static_partitioning_block_005_output", "w")
		elif testMethod == 1 and testPattern == 1 and failureRate == 0.002:
			outfile = open ("Static_partitioning_block_002_output", "w")
		elif testMethod == 1 and testPattern == 1 and failureRate == 0.001:
			outfile = open ("Static_partitioning_block_001_output", "w")
			
		elif testMethod == 1 and testPattern == 2 and failureRate == 0.01:
			outfile = open ("Static_partitioning_strip_01_output", "w")
		elif testMethod == 1 and testPattern == 2 and failureRate == 0.005:
			outfile = open ("Static_partitioning_strip_005_output", "w")
		elif testMethod == 1 and testPattern == 2 and failureRate == 0.002:
			outfile = open ("Static_partitioning_strip_002_output", "w")
		elif testMethod == 1 and testPattern == 2 and failureRate == 0.001:
			outfile = open ("Static_partitioning_strip_001_output", "w")
			
		elif testMethod == 1 and testPattern == 3 and failureRate == 0.01:
			outfile = open ("Static_partitioning_point_01_output", "w")
		elif testMethod == 1 and testPattern == 3 and failureRate == 0.005:
			outfile = open ("Static_partitioning_point_005_output", "w")
		elif testMethod == 1 and testPattern == 3 and failureRate == 0.002:
			outfile = open ("Static_partitioning_point_002_output", "w")
		elif testMethod == 1 and testPattern == 3 and failureRate == 0.001:
			outfile = open ("Static_partitioning_point_001_output", "w")
			
		elif testMethod == 2 and testPattern == 1 and failureRate == 0.01:
			outfile = open ("Distance_Static_partitioning_block_01_output", "w")
		elif testMethod == 2 and testPattern == 1 and failureRate == 0.005:
			outfile = open ("Distance_Static_partitioning_block_005_output", "w")
		elif testMethod == 2 and testPattern == 1 and failureRate == 0.002:
			outfile = open ("Distance_Static_partitioning_block_002_output", "w")
		elif testMethod == 2 and testPattern == 1 and failureRate == 0.001:
			outfile = open ("Distance_Static_partitioning_block_001_output", "w")
			
		elif testMethod == 2 and testPattern == 2 and failureRate == 0.01:
			outfile = open ("Distance_Static_partitioning_strip_01_output", "w")
		elif testMethod == 2 and testPattern == 2 and failureRate == 0.005:
			outfile = open ("Distance_Static_partitioning_strip_005_output", "w")
		elif testMethod == 2 and testPattern == 2 and failureRate == 0.002:
			outfile = open ("Distance_Static_partitioning_strip_002_output", "w")
		elif testMethod == 2 and testPattern == 2 and failureRate == 0.001:
			outfile = open ("Distance_Static_partitioning_strip_001_output", "w")

		elif testMethod == 2 and testPattern == 3 and failureRate == 0.01:
			outfile = open ("Distance_Static_partitioning_point_01_output", "w")
		elif testMethod == 2 and testPattern == 3 and failureRate == 0.005:
			outfile = open ("Distance_Static_partitioning_point_005_output", "w")
		elif testMethod == 2 and testPattern == 3 and failureRate == 0.002:
			outfile = open ("Distance_Static_partitioning_point_002_output", "w")
		elif testMethod == 2 and testPattern == 3 and failureRate == 0.001:
			outfile = open ("Distance_Static_partitioning_point_001_output", "w")


		total = 0

		start_time = time.time()		

		for i in range(5000):
			curCount = test(testMethod, testPattern, 100, failureRate) + 1
			total = total + curCount
#			outfile.write(str(curCount) + "\n")
			if (i == 0):
				max = curCount
				min = curCount
			else:
				if (max < curCount):
					max = curCount
				if (min > curCount):
					min = curCount
			print(i)
		outfile.write("Running time: " + str(time.time() - start_time) + "\n")
		outfile.write("Total is " + str(total) + "\n")
		outfile.write("Average is " + str(total / 5000) + "\n")
		outfile.write("Max is " + str(max) + "\n")
		outfile.write("Min is " + str(min) + "\n")
		outfile.close()
			
	elif userIn == "4":
		printCurrentSetting()
	else:
		print("Invalid input.\n")
		
	printMainMenu()
	
	userIn = input("\nSelect menu: ")